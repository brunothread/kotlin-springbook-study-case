package com.mercadolivro.enums

enum class Errors(val code: String, val message:String) {

    E0001("0001", "Customer [%s] not exists"),
    E0002("0002", "Book [%s] not exists")

}