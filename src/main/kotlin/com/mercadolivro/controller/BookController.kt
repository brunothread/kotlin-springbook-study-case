package com.mercadolivro.controller

import com.mercadolivro.controller.request.PostBookRequest
import com.mercadolivro.controller.request.PutBookRequest
import com.mercadolivro.extension.toBookModel
import com.mercadolivro.model.BookModel
import com.mercadolivro.service.BookService
import com.mercadolivro.service.CustomerService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("book")
class BookController(val customerService: CustomerService, val bookService: BookService) {

    @PostMapping
    fun create(@RequestBody request: PostBookRequest){
        val customer = customerService.getCustomerById(request.customerId!!)
        bookService.create(request.toBookModel(customer))
    }

    @GetMapping
    fun findAll(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookModel>
    {
        return bookService.findAll(pageable)
    }

    @GetMapping("/active")
    fun findActiveBooks():MutableIterable<BookModel> = bookService.findActiveBooks()

    @GetMapping("/{id}")
    fun findById(@PathVariable id:Int):BookModel = bookService.findById(id)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id:Int) = bookService.delete(id)

    @PutMapping("/{id}")
    fun update(@PathVariable id:Int, @RequestBody bookRequest: PutBookRequest) {
        val book = bookService.findById(id)
        bookService.update(bookRequest.toBookModel(book))
    }
}
