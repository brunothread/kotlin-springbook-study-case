package com.mercadolivro.controller.request

import com.sun.istack.NotNull
import javax.validation.constraints.Positive

data class PostPurchaseRequest(

    @NotNull
    @Positive
    val customerId: Int,

    @NotNull
    val bookIds : Set<Int>
)
