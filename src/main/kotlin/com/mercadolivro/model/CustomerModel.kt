package com.mercadolivro.model

import com.mercadolivro.enums.Profile
import javax.persistence.*

@Entity(name = "Customer")
data class CustomerModel(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    @Column
    var name: String,
    @Column
    var email: String,

    @Column
    val password: String,

    @CollectionTable(name = "customer_roles", joinColumns = [JoinColumn(name = "customer_id")])
    @ElementCollection(
        targetClass = Profile::class,
        fetch = FetchType.EAGER
    )//td vez que for buscar um customer, traz o role
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    var roles: Set<Profile> = setOf()
//    @Column
//    var status: CustomerStatus?f
)