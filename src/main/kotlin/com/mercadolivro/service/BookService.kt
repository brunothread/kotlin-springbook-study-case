package com.mercadolivro.service

import com.mercadolivro.controller.exception.NotFoundException
import com.mercadolivro.enums.BookStatus
import com.mercadolivro.enums.Errors
import com.mercadolivro.model.BookModel
import com.mercadolivro.model.CustomerModel
import com.mercadolivro.repository.BookRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class BookService(val bookRepository: BookRepository) {
    fun create(book: BookModel) {
        bookRepository.save(book)
    }

    fun findAll(pageable: Pageable): Page<BookModel> {
        return bookRepository.findAll(pageable)
    }

    fun findActiveBooks(): MutableIterable<BookModel> = bookRepository.findByStatus(BookStatus.ATIVO)

    fun findById(id: Int): BookModel {
        return bookRepository.findById(id).orElseThrow { NotFoundException(Errors.E0002.message.format(id), "E0002") }
    }

    fun delete(id: Int) {
        val book = findById(id)
        book.status = BookStatus.CANCELADO
        bookRepository.save(book)
    }

    fun update(book: BookModel) = bookRepository.save(book)
    fun findAllByIds(bookIds: Set<Int>): List<BookModel> = bookRepository.findAllById(bookIds).toList()

    fun purchase(books: MutableList<BookModel>) {
        books.map{
            it.status = BookStatus.VENDIDO
        }
        bookRepository.saveAll(books)
    }
//    fun deleteByCustomer(customer: CustomerModel) {
//        val books = bookRepository.findByCustomer(customer)
//        for(book in books){
//            book.status = BookStatus.DELETADO
//        }
//        bookRepository.saveAll(books)
//    }

}
