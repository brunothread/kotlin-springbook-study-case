package com.mercadolivro.controller.request

import com.mercadolivro.validation.EmailAvailable
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

data class PostCustomerRequest(

    @NotEmpty
    var name: String,

    @Email
    @EmailAvailable
    var email: String,

    @NotEmpty
    var password:String

)