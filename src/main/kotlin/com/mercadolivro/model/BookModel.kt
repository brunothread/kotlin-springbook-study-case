package com.mercadolivro.model

import com.mercadolivro.enums.BookStatus
import java.math.BigDecimal
import javax.persistence.*

@Entity(name = "Book")
data class BookModel(

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int?,
    @Column
    var name: String,
    @Column
    var price: BigDecimal,

    @Column
    @Enumerated(EnumType.STRING)
    var status: BookStatus? = null,
//
//    @Column(name = "customer")
//    //muitos livros pertencem a 1 usuario
////    @ManyToOne
////    @JoinColumn(name = "customer_id")
//    var customer: CustomerModel? = null
)