package com.mercadolivro.service

import com.mercadolivro.controller.exception.NotFoundException
import com.mercadolivro.enums.Profile
import com.mercadolivro.model.CustomerModel
import com.mercadolivro.repository.CustomerRepository
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

@ExtendWith(MockKExtension::class)
class CustomerServiceTest {

    @MockK
    private lateinit var customerRepository: CustomerRepository

    @MockK
    private lateinit var bookService: BookService

    @MockK
    private lateinit var bCrypt: BCryptPasswordEncoder

    @InjectMockKs
    private lateinit var customerService: CustomerService

    @Test
    fun `should return all customers`() {
        val fakeCustomers = listOf(buildCustomer(), buildCustomer())

        every { customerRepository.findAll() } returns fakeCustomers

        val customers = customerService.getAll(null)

        assertEquals(fakeCustomers, customers)
        verify(exactly = 1) { customerRepository.findAll() }

        verify(exactly = 0) { customerRepository.findByNameContaining(any()) }

    }

    @Test
    fun `should return customers when name is informed`() {
        val name = Math.random().toString()
        val fakeCustomers = listOf(buildCustomer(), buildCustomer())

        every { customerRepository.findByNameContaining(name) } returns fakeCustomers

        val customers = customerService.getAll(name)

        assertEquals(fakeCustomers, customers)
        verify(exactly = 0) { customerRepository.findAll() }

        verify(exactly = 1) { customerRepository.findByNameContaining(name) }

    }

    @Test
    fun `create a customer and encrypt pass`() {
        val password1 = Math.random().toString()
        // passa a senha
        val fakeCustomer = buildCustomer(password = password1)
        val fakePassword = UUID.randomUUID().toString()
        /// encripta a senha
        val customerEncrypted = fakeCustomer.copy(password = fakePassword)
        /// salva o usuario encriptado
        every { customerRepository.save(customerEncrypted) } returns fakeCustomer
        every { bCrypt.encode(any()) } returns fakePassword

        customerService.create(fakeCustomer)

        verify(exactly = 1) { customerRepository.save(customerEncrypted) }
        /// encripta somente o pass
        verify(exactly = 1) { bCrypt.encode(password1) }
    }

    @Test
    fun `return customer found by Id`() {
        /// mocks
        val id = Random().nextInt()
        val fakeCustomer = buildCustomer(id = id)
        every { customerRepository.findById(any()) } returns Optional.of(fakeCustomer)
        //real method
        val customerFound = customerService.getCustomerById(id)

        assertEquals(fakeCustomer, customerFound)
        verify(exactly = 1) { customerRepository.findById(id) }
    }

    @Test
    fun `should throw exception when trying to find by id`() {
        /// mocks
        val id = Random().nextInt()
        every { customerRepository.findById(id) } returns Optional.empty()

        //real method
        val error = assertThrows<NotFoundException> { customerService.getCustomerById(id) }

        assertEquals("Customer [${id}] not exists", error.message)
        assertEquals("E0001", error.errorCode)
        verify(exactly = 1) { customerRepository.findById(id) }

    }

    fun buildCustomer(
        id: Int? = null,
        name: String = "customerName",
        email: String = "${UUID.randomUUID()}@email.com",
        password: String = "password"
    ) = CustomerModel(id, name, email, password, roles = setOf(Profile.CUSTOMER))

}